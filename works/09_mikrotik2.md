/EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Determinamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt08
#Guardamos nuestro progreso
/system backup save name="20170321_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

[admin@infMKT08] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
[admin@infMKT08] > /system identity set name=mkt08
[admin@mkt08] > /system backup save name="20170317_zeroconf"

##--resetamos para poder volver a cargar el backup con la siguiente ordre

[admin@mkt08] > reset-configuration     

Dangerous! Reset anyway? [y/N]: 
y
system configuration will be reset
Connection to 192.168.88.1 closed by remote host.
Connection to 192.168.88.1 closed.
NOTA:al resetar el MKT los valores puestos regresan al los de predefinidos
por el router(como se puede ver en la siguiente orden):

[admin@MikroTik] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:BC:BF:A2

---buscamos el nombre del bakup que cambiamos

[admin@MikroTik] > file print  
 # NAME                                                 TYPE                                                      SIZE CREATION-TIME       
 0 skins                                                directory                                                      jan/01/1970 00:00:01
 1 auto-before-reset.backup                             backup                                                 24.6KiB jan/02/1970 01:06:53
 2 autosupout.rif                                       .rif file                                             446.7KiB jan/02/1970 00:24:21
 3 20170317_zeroconf.backup                             backup                                                 24.6KiB jan/02/1970 01:04:21

--Ahora cargamos el bakup que guardamos antes.

[admin@MikroTik] > system backup load name=20170317_zeroconf.backup 
password: *****
Restore and reboot? [y/N]: 
NOTA:al volverlo a cargar cambia de calve i tenemo0s que borrar la anterior parai
que no nos moleste (i haci cada vez que hagamos el backup):

[user1@j08 netswithlinux]$ rm -f ~/.ssh/known_hosts 

---los valores del bakup se restauren

[admin@mkt08] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5



### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free1XX"
/interface wireless add ssid="private2XX" master-interface=wlan1
```
i
admin@mkt08] > interface print          
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5  X  wlan2                               wlan                   1600       2290 6E:3B:6B:BC:BF:A5



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

---HAbilitar les wifis
[admin@mkt08] > interface wireless enable wlan1 
[admin@mkt08] > interface wireless enable wlan2 

-cambiar el nombre de las wifi
[admin@mkt08] > interface set name="wFree" wlan1
[admin@mkt08] > interface set name="wPrivate" wlan2 

## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   

/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX


/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      


/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX

/interface print


```

### [ejercicio3] Comenta cada una de estas líneas de la configuración
#anterior para que quede bien documentado


#añadimos les vlans con su respectivo nombre i interifcie:

->/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
->/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4


#añadimos los bridges con su respectivo nombre 

->/interface bridge add name=br-vlan1XX
->/interface bridge add name=br-vlan2XX

#Añadimos los puertos a nuestras interficies de red. (puertos 3 y 4)

->/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
->/interface bridge port add interface=eth3 bridge=br-vlan1XX
->/interface bridge port add interface=wFree  bridge=br-vlan1XX      

# Añadimos los puertos a nuestras interficies de red(4 para la segunda
vlan y para el wifi privado).

->/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX

->/interface bridge port add interface=wPrivate   bridge=br-vlan2XX
#
/interface print (mostramos inbterfaces)

Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU
 0     eth1                                ether            1500  1598
 1  R  eth2                                ether            1500  1598
 2   S eth3                                ether            1500  1598
 3     eth4                                ether            1500  1598
 4   S wFree                               wlan             1500  1600
 5   S wPrivate                            wlan             1500  1600
 6  R  br-vlan109                          bridge           1500  1594
 7  R  br-vlan209                          bridge           1500  1594
 8   S eth4-vlan109                        vlan             1500  1594
 9   S eth4-vlan209                        vlan             1500  1594


### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
#y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
#base de tu ordenador que hay conectividad (puedes hacer ping) configurando
#unas ips cableadas. Cuando lo hayas conseguido haz un backup de la #configuración

ip addres add interface=eth4-vlan109 address=172.17.109.1/24
ip addres add interface=eth4-vlan209 address=172.17.209.1/24

ping address=192.168.88.1 (Hacemos ping)(en el router)
ping 192.168.88.1
#Hacemos backup de nuestros datos.
system backup save name=20170323_config_inicial.backup 

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
#.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
#Crear reglas de nat para salir a internet. 

#Crear el servidor DHCP:
ip pool add ranges=172.17.109.101-172.17.109.250 name=range_public

ip dhcp-server add interface=eth1
ip dhcp-server add interface=eth2
ip dhcp-server add interface=eth3
ip dhcp-server add interface=eth4

ip dhcp-server print

ip dhcp-server set 0 disabled=no

ip dhcp-server print

ip dhcp-server network add gateway=172.17.109.1 dns-server=8.8.8.8 netmask=24
domain=casa.com addres=172.17.109.0/24
### [ejercicio6] Activar redes wifi y dar seguridad wpa2

[admin@mkt09] > interface wireless set wFree ssid=mktf09
[admin@mkt09] > interface wireless set wPrivate ssid=mktp09
[admin@mkt09] > interface wireless enable wFree            
[admin@mkt09] > interface wireless enable wPrivate
[admin@mkt09] > interface wireless security-profiles add wpa2-pre-shared-key="luiscampos" name=luis


### [ejercicio6b] Opcional (monar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar


### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

