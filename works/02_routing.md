A partir del esquema de redes e ips del aula:

                                                    +------------ internet
                                                    |
                                                +-------+
                       +------------------------+ PROFE +---------------------+
                       |                        +-------+                     |
      2.1.1.0/24       |.254                                 2.1.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.6        |.5        |.4                              |.3        |.2        |.1
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC06 |    |PC05 |    |PC04 |                          |PC03 |    |PC02 |    |PC01 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.2.1.0/24       |.254                                 2.2.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.12       |.11       |.10                             |.9        |.8        |.7
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC12 |    |PC11 |    |PC10 |                          |PC09 |    |PC08 |    |PC07 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.3.1.0/24       |.254                                 2.3.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.18       |.17       |.16                             |.15       |.14       |.13
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC18 |    |PC17 |    |PC16 |                          |PC15 |    |PC14 |    |PC13 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.4.1.0/24       |.254                                 2.4.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.24       |.23       |.22                             |.21       |.20       |.19
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC24 |    |PC23 |    |PC22 |                          |PC21 |    |PC20 |    |PC19 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.5.1.0/24       |.254                                 2.5.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.30       |.29       |.28                             |.27       |.26       |.25
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC30 |    |PC29 |    |PC28 |                          |PC27 |    |PC26 |    |PC25 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         
###### 0. Identifica cual es tu PC, que ips/máscaras ha de llevar sobre qué interfaces

*My IP: 2.2.2.9/24  
         
###### 1. Lista de órdenes para poder introducir manualmente las direcciones ip de tu puesto de trabajo con la orden ip y evitar que el NetworkManager o configuraciones previas interfieran en estas configuraciones

systemctl stop NetworkManager

ip a a 2.2.2.9/24 dev enp2s0

###### 2. Lista de órdenes para configurar el PC de tu fila como router con un comentario explicando lo que hace cada línea

ip r a (añadimos las rutas)
echo1 > /proc/sys/net/ipv4/ip_forward (convertir en router)



###### 3. Explicación de cada línea de la salida del comando ip route show

1- Destino

2- puerto

3- Máscara

4- Flags

5 - Interfaz de red



###### 4. Hacer un ping a un pc de tu misma fila y al PC19 y al PC24. 

**Capturar los paquetes con wireshark y guardar la captura. Analizarla 
observando el campo TTL del protocolo IP. Explicar los diferentes valores
de ese campo en función de la ip a la que se ha hecho el ping**

###### 5. Hacer un traceroute y un mtr al 8.8.8.8 Explicar a partir de que ip se corta y por qué. 

traceroute 8.8.8.8

###### 6. Explica que es el enmascaramiento y cómo se aplicaría en el ordenador del profe para que de salida a internet

Enmascaramiento -> El enmascaramiento IP es una función de red que permite la conexión a otros miembros de la red a Internet a través de la conexión que ya posee la máquina que soporta el enmascaramiento
