#### 1. ip a

*Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
->Paramos el servicio NetworkManager -> dhclient -r

->Eliminar todas las rutas-> ip r f all

->Eliminar todas las direcciones ip -> ip a f dev enp2s0 (quitamos ip de esta interfaz)
										ip a f dev lo (quitamos la ip de esta interfaz)
										
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24,-> ip a a 2.2.2.2/24 dev enp2s0

3.3.3.3/16, -> ip a a 3.3.3.3/16 dev enp2s0

4.4.4.4/25 -> ip a a 4.4.4.4/25 dev enp2s0

Consulta la tabla de rutas de tu equipo-> ip r

*2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 

3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 

4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4


Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 ->contesta porque es una dirección red que existe.Es una de mis direcciones ip.

2.2.2.254 ->  Destination host is unreachable. Porque la red es valida, pero nadie contesta

2.2.5.2 -> Network is unreachable. La ip no esta dentro de lam máscara.Mi ip es mascara 24.

3.3.3.35 -> Destination Host is unreachable. La ip es valida ,nadie contesta.

3.3.200.45 -> Destination Host is unreachable. La ip  es valida, pero nadie contesta.

4.4.4.8 -> Destination Host is unreachable. La ip es valida, pero nadie me contesta, nadie tiene esa ip.

4.4.4.132 -> Network is unreachable. La ip no es valida, esta fuera de rango.

#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

->ip a f dev enp2s0

->ip r f enp2s0

Conecta una segunda interfaz de red por el puerto usb

Cambiale el nombre a usb0

->ip link set usb0 down

->ip link set eth0 name 9

->ip link set eth0 up

Modifica la dirección MAC

-> ip link set 9 down

-> ip link set 9 address 00:11:22:33:44:55

-> ip link set 9 up

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.
-> ip a a 5.5.5.5/24 dev usb0

-> ip a a 7.7.7.7/24 dev enp2s0

Observa la tabla de rutas

->ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:b6:e9:07 brd ff:ff:ff:ff:ff:ff
    inet 192.168.3.9/16 brd 192.168.255.255 scope global dynamic enp2s0
       valid_lft 19313sec preferred_lft 19313sec
    inet 7.7.7.7/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 5.5.5.5/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet6 fe80::16da:e9ff:feb6:e907/64 scope link 
       valid_lft forever preferred_lft forever


#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

->ip r f all

->ip a f dev enp2s0

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

->ip a a 172.16.99.9/24

Lanzar iperf en modo servidor en cada ordenador

Comprueba con netstat en qué puerto escucha

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

