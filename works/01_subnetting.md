# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 10101100.00010000.00000100.00010111 | 172.16.4.127 | Min:172.16.4.1 Max:172.16.4.126
174.187.55.6/23 | 10101110.10111011.00110111.00000110 | 174.187.55.255 | Min:174.187.54.1 Max:174.187.55.254 
10.0.25.253/18 | 00001010.00000000.00011001.11111101 | 10.0.63.255 | Min:10.0.0.1 Max:10.0.63.254 
209.165.201.30/27 | 11010001.10100101.11001001.00011110 | 209.165.201.31 | Min:209.165.201.1 Max:209.165.201.30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.
*Nova màscara en decimal: 255.255.254.0 (en binari és:11111111.11111111.11111110.00000000)

*Nova màscara :172.28.0.0/23 



##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

*Màscara en binari: 11111111.11111111.11111000.00000000

*Màscara en decimal: 255.255.248.0

*10.192.0.0/21


b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

*No es poden fer subxarxes de 1500 dispositius amb la màscara /10


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

*Nova màscara en binari: 11111111.11111111.11110000.00000000

*Nova màscara en decimal: 255.255.240.0

*Nova màscara: 10.192.0.0/20

##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius

a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

*Nova màscara en decimal: 255.255.240.0

*Nova màscara en binari: 11111111.11111111.11110000.00000000

*Màscara: 10.128.0.0/20


b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

*Nova màscara en decimal: 255.255.252.0

*Nova màscara en binri:11111111.11111111.11111100.00000000

*Màscara: 10.128.0.0/22


c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

*2^m -2 = hosts

*2^10 -2 = 1022 dispositius es poden adreçar per subxarxa,per tant es podràn adreçar els 650 dispositius

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

*Si que es poden fer dues subxarxes de 1625 dispositius.

*2^m -2 = hosts

*2^12 -2 = 4096  dispositius es poden fer per subred.

*2^4= 16 subredes
##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

*3 bits


b) Dóna la nova MX, tant en format decimal com en binari.

*Nova màscara en binari:11111111.11111110.00000000.00000000

*Nova màscara en decimal: 255.254.0.0 

*Nova màscara: 172.16.0.0/15


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 172.16.0.0 | 172.17.255.255 | 172.16.0.1 - 172.17.255.254
Xarxa 2 | 172.18.0.0 | 172.18.255.255 | 172.18.0.1 - 172.19.255.254
Xarxa 3 | 172.20.0.0 | 172.21.255.255 | 172.20.0.1 - 172.21.255.254
Xarxa 4 | 172.22.0.0 | 172.23.255.255 | 172.22.0.1 - 172.23.255.254
Xarxa 5 | 172.24.0.0 | 172.25.255.255 | 172.24.0.1 - 172.25.255.254
Xarxa 6 | 172.26.0.0 | 172.27.255.255 | 172.26.0.1 - 172.27.255.254


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

*Com es poden acivar 19 vits en el host, es podrien fer 524288 subxarxes perquè 2^19 = 524288

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

*En el cas de l'apartat a, com queden 20 bits disponibles en el host. 2^20 -2 = 1048572 dispositius per subxarxa

*En el cas de l'apartat b, com queden 17 bits disponibles en el host. 2^17 -2 = 131070 dispositius per subxarxa


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

*3 bits es necessiten


b) Dóna la nova MX, tant en format decimal com en binari.

*Nova màscara en binari:11111111.11111111.11111111.11100000

*Nova màscara en decimal: 255.255.255.254

*Màscara: 192.198.1.0/27


c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 192.168.1.0 | 192.168.1.31 | 192.168.1.1 - 192.168.1.30 |
Xarxa 2 | 192.168.1.32 | 192.168.1.63 | 192.168.1.33 - 192.168.1.62 |
Xarxa 3 | 192.168.1.64 | 192.168.1.95 | 192.168.1.65 - 192.168.1.94 |
Xarxa 4 | 192.168.1.96 | 192.168.1.127 | 192.168.1.97 - 192.168.1.126 |



d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

*Podriem utilitzar 32 subxarxes. 2^5 = 32 subxarxes. El número 5 son el número de bits que es poden activar del host.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

*Quan és màscara 24 -> 2^m -2 = hosts ; 2^8 -2 = 254 dispositius per subxarxa

*Quan és màscara 27 -> 2^5 -2 = 30 dispositius per subxarxa
